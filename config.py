api_key = "API_KEY" # telegram bot API key
rand_response_freq = 3 # percentage of the time markov bot will respond to a regular message
message_max_words = 100 # max number of words in a /markov
essay_min_words = 100 # min number of words in a /markovessay
markov_cache_file = "FULL/FILEPATH/markov_data.pkl" # markov cache file for persistence

markov_ocr_enabled = False # Enable or disable ocr functionality
markov_print_ocr = True # Send OCR text back to the chat as a message 
markov_image_file = "FULL/FILEPATH/WITH/FILENAME.jpg" # image file for ocr
markov_image_file_grayscale = "FULL/FILEPATH/WITH/DIFFERENT_FILENAME.jpg" # grayscale image file for ocr
